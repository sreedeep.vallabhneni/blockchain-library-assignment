#!/bin/bash

function upgradeNumberByPosition { 
  echo | string=$1 position=$2 \
    awk -v FS=. -v OFS=.  '{ 
      $0=ENVIRON["string"]; 
      $ENVIRON["position"]++; 
      print;
   }';
}

docker rm -f `(docker ps -qa --latest)`
fileName=`jq '.name' package.json`

oldVersion=`jq '.version' package.json`

newVersion=$(upgradeNumberByPosition "$oldVersion" 3)

tmp=$(mktemp)
jq --arg v "${newVersion:1}" '.version = $v' package.json > "$tmp" && mv "$tmp" package.json

rm ${fileName:1:-1}@${oldVersion:1:-1}.bna
composer archive create -t dir -n .
composer network install --card PeerAdmin@hlfv1 --archiveFile ${fileName:1:-1}@${newVersion:1}.bna
composer network upgrade -c PeerAdmin@hlfv1 -n ${fileName:1:-1} -V ${newVersion:1}
x-terminal-emulator -e docker logs `(docker ps -qa --latest)`  -f
composer-playground
