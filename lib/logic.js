/**
 * Change status of an asset
 * @param {org.ggktech.library.RequestBook} requestBook - the trade to be processed
 * @transaction
 */
async function bookRequest(requestBook) {
    let book = requestBook.book
    if(book == null){
        throw new Error('No books found with the title!')
    }
    if(requestBook.toUser.totalSlots <= requestBook.toUser.books_received.length){
        throw new Error('User cannot borrow more than '+requestBook.toUser.totalSlots+' books!');
    }

    for(let bookListing of book.bookListings){
        if(await verifyBookAvailability(bookListing)){
            await bookTransfer(bookListing, requestBook.toUser);
            return;
        }
    }
    throw new Error('No available books!')
}

async function verifyBookAvailability(bookListing) {
    let lastBookTransactions = await query('getlastBookBookTransactions', 
    {bookListing: `resource:org.ggktech.library.BookListing#${bookListing.Id}`});
    // {bookListing: `${bookListing}`});
    if(lastBookTransactions.length === 0){
        return true;
    } 
    let lastBookTransaction = lastBookTransactions[0];
    if(lastBookTransaction.date_submission == null){ 
        return false;
    } else {
        // book is with library
        // throw new Error('date_submission!'+date_submission)
        return true;
    }
}

async function bookTransfer(bookListing, toUser) {
    let factory = getFactory();
    let results = await query('getBookTransactions');
    let count = results.length;

    let  newTransaction = factory.newResource('org.ggktech.library', 'BookTransaction', (count+1).toString());
    newTransaction.toUser = toUser;
    newTransaction.Id = (count+1).toString();

    newTransaction.bookListing = bookListing;
    newTransaction.toUser = toUser;
    newTransaction.date_received = new Date();

    let bookTransactionRegistry = await getAssetRegistry('org.ggktech.library.BookTransaction');
    await bookTransactionRegistry.add(newTransaction);

    let bookListingRegistry = await getAssetRegistry('org.ggktech.library.BookListing');
    bookListing.bookTransactions.push(newTransaction);
    await bookListingRegistry.update(newTransaction.bookListing);

    let userRegistry = await getParticipantRegistry('org.ggktech.library.Users');

    toUser.books_received.push(bookListing);
    await userRegistry.update(toUser);
}

/**
 * Change status of an asset
 * @param {org.ggktech.library.ReturnAndSettle} returnAndSettle - the trade to be processed
 * @transaction
 */
async function bookReturn(returnAndSettle) {
    let lastBookTransactions = await query('getlastBookBookTransactions', 
    {bookListing: `resource:org.ggktech.library.BookListing#${returnAndSettle.bookListing.Id}`});
    // {bookListing: `${bookListing}`});
    if(lastBookTransactions.length === 0){
        throw new Error('Book has never been issued!')
    } 
    let lastBookTransaction = lastBookTransactions[0];
    
    // throw new Error('Book has nev'+lastBookTransaction.date_received+'er been issued!'+lastBookTransaction.date_submission+'aaa');
    if(lastBookTransaction.date_received !== null && !lastBookTransaction.date_submission){ 
        lastBookTransaction.date_submission = new Date();
        let bookTransactionRegistry = await getAssetRegistry('org.ggktech.library.BookTransaction');
        await bookTransactionRegistry.update(lastBookTransaction);

        let userRegistry = await getParticipantRegistry('org.ggktech.library.Users');
        // throw new Error('Book not is   '+lastBookTransaction.toUser+'   sued to user!'+lastBookTransaction.toUser.books_received);
        let user = await userRegistry.get(lastBookTransaction.toUser.getIdentifier());

        removeAssetFromArrayById(user.books_received, lastBookTransaction.bookListing); 
        await userRegistry.update(user);
    } else {
        throw new Error('Book not issued to user!');
    }
}

function removeAssetFromArrayById(array, elem) {
    for(let i=0;i<array.length;i++){
        if(array[i].Id === elem.Id){
            array.splice(i, 1);
        }
    }
}
